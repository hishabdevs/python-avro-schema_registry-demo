This is just an example file to Produce and Consume avro formatted message in python to a particular Kafka Topic

First install confluent-kafka-python package by the following command

pip install confluent-kafka

Run the following command to produce

python demo-producer.py

Run the following command to consume

python demo-consumer.py